﻿using UnityEngine;
using UnityEditor;


//#3
/**
	* An ui for editing event for better debugging!
	(enables an event without playing the game directly for events that extended from gameEvent class) 
	clock on extended events to view the ui(create>new>GameEvent)
 */
 [CustomEditor(typeof(GameEvent))]
public class GameEventEditor : Editor {


	public override void OnInspectorGUI(){//AA:when inpector ui is ready
			
		base.OnInspectorGUI();

		GUI.enabled=Application.isPlaying;//if game is playing enable the ui buttons

		GameEvent e = target as GameEvent;

		if(GUILayout.Button("Raise The Event!")){
			e.Raise();//runs the onEventRaised method(by gamevent)
		}

	}


}
