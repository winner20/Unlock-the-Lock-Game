﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameData GameData;

    public GameEvent OnLevelChange;

    public GameObject EndView;

    void Start()
    {
        GameData.ResetLevel();
    }

    public void LoadNextLevelIfPossible()
    {//if user is changing level in menu it should not jump to next level!
        if (GameData.MaxUnlockedLevel < GameData.CurrentLevel + 1)
        {
            return;
        }
        LoadLastOrNextLevel(true);
    }

    public void LoadLastOrNextLevel(bool NextLevel)
    {
        Debug.Log("Restarting or loading a level...");
        if (NextLevel)
        {

            if (GameData.CurrentLevel < GameData.MaxUnlockableLevel){
                GameData.SetCurrentLevel(GameData.CurrentLevel + 1);
                
            }else EndView.SetActive(true);
                            

        }
        else if (GameData.CurrentLevel > 1)
        {

            GameData.SetCurrentLevel(GameData.CurrentLevel - 1);
        }
        OnLevelChange.Raise();
        GameData.ResetLevel();

    }
    public void RestartLevel()
    {
        GameData.ResetLevel();
    }

    public void DecRemainingDots()
    {
        GameData.RemainingDots--;
    }





}





