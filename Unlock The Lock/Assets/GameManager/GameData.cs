﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GameData : ScriptableObject {

	//current level number
	private int _currentLevel;
	public int MaxUnlockableLevel=50;

	public int RemainingDots;
	//current stars count
	private int stars;

	public bool isColliding=false;

    public int speed = 120;
	public int DefSpeed=120;

	public bool IsRunning=false;

	///to disable touch while menu is open
	public bool isMenuShowing=false;
	public bool isSaveMeMenuShowing=false;
	
	public bool FirstTap=true;
	public bool failed=false;

	public void ResetLevel(){
		Debug.Log("Reseting level..");
		RemainingDots = CurrentLevel;
		IsRunning=false;
		FirstTap=true;
		speed=DefSpeed;

	}

	
	public void SetCurrentLevel(int lvl){
		Debug.Log("Setting current level to "+lvl);
		PlayerPrefs.SetInt("lvl",lvl);
		if (MaxUnlockedLevel<lvl)
		{
			PlayerPrefs.SetInt("maxUnlocked",lvl);
		}
	}

	public void AppendStar(int val){
		PlayerPrefs.SetInt("stars",Stars+val);
	}
	public void DecStar(int value){
		PlayerPrefs.SetInt("stars",Stars-value);
	}

	public int CurrentLevel{
		get{
			return PlayerPrefs.GetInt("lvl",1);
		}
	}
	public int Stars{
			get{
			return PlayerPrefs.GetInt("stars",20);
		}
	}
	public int MaxUnlockedLevel{
		get{
			return PlayerPrefs.GetInt("maxUnlocked",1);
		}
	}

}
