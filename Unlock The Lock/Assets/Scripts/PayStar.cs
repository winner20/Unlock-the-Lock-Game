﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PayStar : MonoBehaviour
{

    public GameData GameData;
    public GameEvent SaveMeEvent;

    public GameEvent StarScoredEvent;//to inform the star text ui that count is changing

    public Text Text2Warn;




    public void Pay(int stars)
    {
        if (GameData.Stars < stars)
            Text2Warn.text = "): ﺖﺴﯿﻧ ﯽﻓﺎﮐ ﻪﻣﺍﺩﺍ ﯼﺍﺮﺑ ﻩﺭﺎﺘﺳ ﻥﺍﺰﯿﻣ";
        else
        {
            SaveMeEvent.Raise();
            GameData.isSaveMeMenuShowing = false;
            GameData.DecStar(stars);
            if (Text2Warn.text != "") { Text2Warn.text = ""; }
            StarScoredEvent.Raise();
        }

    }


}
