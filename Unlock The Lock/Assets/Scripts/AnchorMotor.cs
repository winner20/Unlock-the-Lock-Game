﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnchorMotor : MonoBehaviour
{


    private Transform _anchori;

    public Direction Direction = Direction.Clockwise;

    public GameData GameData;//game data instance object

    public GameEvent OnTapEvent;//to inform listeners

    Vector3 DefPos;

    public GameEvent OnPaddleReset;


    public int maxSpeed;


    // Use this for initialization
    void Start()
    {

        _anchori = GameObject.FindGameObjectWithTag("Anchor").transform;
        DefPos = GetComponent<Transform>().localPosition;

    }

    // Update is called once per frame
    void Update()
    {


        if (GameData.IsRunning && !GameData.failed//if player is running and not failed 
        && !GameData.isMenuShowing && !GameData.isSaveMeMenuShowing)//if no menu is showing
        {
            transform.RotateAround(
                _anchori.position,
            Vector3.forward,
             GameData.speed * Time.deltaTime * (int)Direction);
        }
        if (TouchHelper.IsTouching(GameData))
        {
            OnTapEvent.Raise();
            if (GameData.isColliding)
            {
                ChangeDirection();
            }
        }
        if (TouchHelper.IsTouching(GameData) && !GameData.IsRunning)
        {
            GameData.IsRunning = true;
        }
    }


    void ChangeDirection()
    {
        if (Direction == Direction.Clockwise)
        {
            Direction = Direction.AntiClockwise;
        }
        else
        {
            Direction = Direction.Clockwise;
        }

    }

    ///moves the paddle to the default position
    public void ResetPaddlePosition()
    {

        transform.localPosition = new Vector3(0, DefPos.y, 0);
        transform.localRotation = Quaternion.identity;
        //transform.rotation= Quaternion.Euler(0f,0f,0f);
        GameData.IsRunning = false;
        GameData.failed = false;
        GameData.FirstTap = true;
        OnPaddleReset.Raise();
                if (Direction == Direction.AntiClockwise )  Direction = Direction.Clockwise;

    }





    //is called in ui
    public void IncSpeed()
    {

        if (GameData.speed < maxSpeed)
        {
            if (GameData.CurrentLevel >3 && GameData.CurrentLevel <= 10)
            {
                GameData.speed += Random.Range(15, 20);
            }else if (GameData.CurrentLevel > 10)
            {
                GameData.speed += Random.Range(8, 20);
                if(GameData.speed > maxSpeed) GameData.speed = maxSpeed;//to avoid over value
            }
        }


    }


}






public enum Direction
{
    Clockwise = -1,
    AntiClockwise = 1
}
