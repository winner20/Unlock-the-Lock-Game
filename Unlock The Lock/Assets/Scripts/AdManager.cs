﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TapsellSDK;

public class AdManager : MonoBehaviour
{

    public GameData _GameData;
    public GameEvent SaveMeEvent;

    public Text txtInfo;

    private bool gotAd = false;



    public GameEvent StarScoredEvent;//to inform the star text ui that count is changing

    // Use this for initialization
    void Start()
    {
        Tapsell.initialize("lqhsqcqnfmqjerbkfttdbstrfmpdnthntropndpeoodirgqedebblejifesimbcbniqkii");
    }

    void Update()
    {
      if(gotAd && !_GameData.isSaveMeMenuShowing)hideBanner();//to prevent showing ad after exiting menu(event based method is not accurate)

    }


    public void showVideoAd()
    {


        Tapsell.requestAd("5c73f8621ac6a50001aaefeb", false,
           (TapsellAd result) =>
           {
           // onAdAvailable

           TapsellAd ad = result; // store this to show the ad later


           TapsellShowOptions showOptions = new TapsellShowOptions();
               showOptions.backDisabled = false;
               showOptions.immersiveMode = false;
               showOptions.rotationMode = TapsellShowOptions.ROTATION_UNLOCKED;
               showOptions.showDialog = true;
               Tapsell.showAd(ad, showOptions);

               Tapsell.setRewardListener((TapsellAdFinishedResult watchResult) =>
              {


        if (watchResult.completed && watchResult.rewarded)
            {
                SaveMeEvent.Raise();
                _GameData.isSaveMeMenuShowing = false;
                _GameData.AppendStar(50);
                StarScoredEvent.Raise();

            }


        }
           );

           },



        (string zoneId) =>
        {
        // onNoAdAvailable
        txtInfo.text = "): ﺪﺸﻧ ﺍﺪﯿﭘ ﺶﯾﺎﻤﻧ ﯼﺍﺮﺑ ﯽﻐﯿﻠﺒﺗ";
        },

        (TapsellError error) =>
        {//error
        txtInfo.text = ")! :ﺩﺍﺩ ﺥﺭ ﯽﯾﺎﻄﺧ ﻪﻧﺎﻔﺳﺎﺘﻣ";
        },

        (string zoneId) =>
        {//no internet
        txtInfo.text = "): ﻩﺭﺍﺪﻧ ﺩﻮﺟﻭ ﺖﻧﺮﺘﻨﯾﺍ ﻪﺑ ﯽﺳﺮﺘﺳﺩ ﺩﺎﯿﻣ ﺮﻈﻨﺑ";
        },
          (TapsellAd result) =>
          {
          // onExpiring
          // this ad is expired, you must download a new ad for this zone
          txtInfo.text = "): ﺖﺴﯿﻧ ﻎﯿﻠﺒﺗ ﺶﯾﺎﻤﻧ ﻥﺎﮑﻣﺍ";//#fix
      }
    );


    }

    
 string bannerZone = "5c73f8ab1ac6a50001aaefec";
    public void showBannerAd()
    {

        if (gotAd)//if one ad loaded already
        {//just show it
            Tapsell.showBannerAd(bannerZone); //shows already loaded ad
            return;
        }

        //receives an ad and shows it
        Tapsell.requestBannerAd(bannerZone, BannerType.BANNER_320x50,
         Gravity.BOTTOM, Gravity.CENTER,
            (string zoneId) =>
            {
               // Debug.Log("Action: onBannerRequestFilledAction");
                gotAd = true;
            },
            (string zoneId) =>
            {
                //Debug.Log("Action: onNoBannerAdAvailableAction");
            },
            (TapsellError tapsellError) =>
            {
               // Debug.Log("Action: onBannerAdErrorAction");
            },
            (string zoneId) =>
            {
               // Debug.Log("Action: onNoNetworkAction");
            },
            (string zoneId) =>
            {
                //Debug.Log("Action: onHideBannerAction");
            });
    }
    public void hideBanner()
    {
        Tapsell.hideBannerAd(bannerZone);
    }

}
