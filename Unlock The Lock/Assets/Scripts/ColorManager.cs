﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorManager : MonoBehaviour {

	public Color[] NormalColor;

	public Color FailColor;



	
	private Camera _cam;

	// Use this for initialization
	void Start () {
		
		_cam=GetComponent<Camera>();
	}
	
	public void setNormalColor(){
		_cam.backgroundColor = NormalColor[Random.Range(1,NormalColor.Length-1)];
	}
	public void setFailColor(){
		_cam.backgroundColor = FailColor;
		
		
	}
}
