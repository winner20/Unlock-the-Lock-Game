﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    public AudioSource _AudioSource;
    public AudioClip DotScoredSFX;
    public AudioClip DotMissedSFX;
    public AudioClip LevelFinishedSFX;
    public AudioClip StarDotScoredSFX;


    public GameData gameData;

    bool SoundEnabled;
    bool blockToggle=true;
   void Start()
   {
       UpdateSoundStateVar();
       Invoke("wait4ui",1.5f);
   }

   void wait4ui(){
        blockToggle=false;
        Debug.Log("Ui is ready to get changed by user");
   }
 

    ///updates SoundEnabled var for one time
    ///instead of accessing it directly from player prefs every frame,called by this class and ui sound toggle

    private void UpdateSoundStateVar()
    {
        SoundEnabled = PlayerPrefs.GetInt("sound", 1) == 1;
    }



    ///forUi(in options)
    public void ToggleSound()
    {
        
        if (blockToggle)
        {  
            //Debug.Log("Sound is automatically toggling,ignoring state...");
            return;
        }
        Debug.Log("Toggling sound...");
        PlayerPrefs.SetInt("sound", PlayerPrefs.GetInt("sound", 1) == 1 ? 0 : 1);    
        UpdateSoundStateVar();
    }


    public void PlayDotScoredSound()
    {

        _AudioSource.clip = DotScoredSFX;
        play();
    }

    public void PlayStarDotScoredSound()
    {

        _AudioSource.clip = StarDotScoredSFX;
        play();
    }

    public void PlayDotMissedSound()
    {
        _AudioSource.clip = DotMissedSFX;
        play();
    }

    public void PlayLevelFinishedSFX()
    {
        _AudioSource.clip = LevelFinishedSFX;
        play();
    }

    //sfx player
    private void play()
    {
        if (!SoundEnabled) return;
        _AudioSource.Play();
    }

}
