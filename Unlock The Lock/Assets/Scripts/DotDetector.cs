﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DotDetector : MonoBehaviour
{
    GameObject collidedDot;

    public GameEvent missedDotEvent;
    public GameEvent scoredDotEvent;

    public GameEvent OnWinEvent;

    public GameEvent StarDotScoredEvent;

    public float LoseThreshold = 0.7f;

    public GameData GameData;//game data instance object



    private GameObject LastCollidedDot;


    // Update is called once per frame
    void Update()
    {


        if (GameData.IsRunning)
        {

            if (LastCollidedDot != null && GetDistanceFromLastDot() > LoseThreshold
            && !GameData.failed)
            {
                GameData.failed = true;
                missedDotEvent.Raise();

            }

            if (TouchHelper.IsTouching(GameData))
            {
                if (collidedDot != null)
                {//Paddle is colliding with dot
                    if (collidedDot.GetComponent<Star>() != null)
                    {
                        StarDotScoredEvent.Raise();
                        GameData.AppendStar(2);

                    }
                    else
                    {
                        scoredDotEvent.Raise();
                    }


                    Destroy(collidedDot);

                    if (GameData.RemainingDots < 1)
                    {
                        OnWinEvent.Raise();
                    }

                }
                else if (!GameData.FirstTap && !GameData.failed)
                {//player clicked at the wrong moment and it wasnt first tap
                    GameData.failed = true;

                    missedDotEvent.Raise();


                }

            }
            if (Input.GetMouseButtonUp(0))
                GameData.FirstTap = false;



        }

    }


    float GetDistanceFromLastDot()
    {//get distance from last collided dot
        return (transform.position - LastCollidedDot.transform.position).magnitude;
    }

    void OnTriggerEnter2D(Collider2D collideInfo)
    {
        collidedDot = collideInfo.gameObject;
        GameData.isColliding = true;
    }

    void OnTriggerExit2D(Collider2D collideInfo)
    {
        LastCollidedDot = collidedDot;


        collidedDot = null;

        GameData.isColliding = false;
    }





}
