﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class WarnTextUi : MonoBehaviour
{

    private Text _Text;

    void Start()
    {
        _Text = GetComponent<Text>();
    }

    public void ResetText()
    {
        if (_Text.text != "")
        {
            _Text.text = "";
        }
    }

}
