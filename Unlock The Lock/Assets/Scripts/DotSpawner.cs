﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DotSpawner : MonoBehaviour
{


    public AnchorMotor Motor;
    public GameObject DotPrefab;
    public GameObject StarDotPrefab;
    public GameData GameData;
    private GameObject SpawnedDot;


    private GameObject MakeRandomDot()
    {
        if (Random.value < 0.3)
        {
            return StarDotPrefab;
        }
        return DotPrefab;
    }

    public void SpawnDot()
    {

     

        if (GameData.RemainingDots > 0)
        {
        if (SpawnedDot)
            {//to remove last dot if already exists			
                Destroy(SpawnedDot);
            }
            var angle = Random.Range(-70, -160);
            SpawnedDot = Instantiate(MakeRandomDot(), Motor.transform.position, Quaternion.identity,
            transform);//makes a dot at the paddle position(because anchor motor is in paddle)
            SpawnedDot.transform.RotateAround(transform.position,
                Vector3.forward, -angle * (int)Motor.Direction);//rotates the dot based on paddle position an against paddle direction!

        }

    }

    // Use this for initialization
    void Start()
    {
        SpawnDot();
    }

}
