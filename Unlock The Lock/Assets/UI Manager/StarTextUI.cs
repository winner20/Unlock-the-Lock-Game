﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StarTextUI : MonoBehaviour {

	public GameData GameData;
	private Text     _text;

	void Start () {
		_text = GetComponent<Text>();
		_text.text = GameData.Stars.ToString();
	}
	
	public void updateText () {
		_text.text = GameData.Stars.ToString();
	}
}
