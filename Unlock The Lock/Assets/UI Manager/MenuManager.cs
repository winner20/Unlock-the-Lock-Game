﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///Shows or hides forms and informs the touch manager to not run the game by touching while menu is open!
public class MenuManager : MonoBehaviour
{

    public GameData gameData;

    public void IsMenuShowing(bool isShowing)
    {
        gameData.isMenuShowing = isShowing;
    }
    public void IsSaveMenuShowing(bool isShowing)
    {
        gameData.isSaveMeMenuShowing = isShowing;
    }
    public void NotifyMenuHided(){
          gameData.isSaveMeMenuShowing = false;
    }
}
