﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RemainingDotsTextUI : MonoBehaviour
{

    public GameData GameData;
    private TextMeshPro _text;

    // Use this for initialization
    void Start()
    {
        _text = GetComponent<TextMeshPro>();
        _text.text = GameData.RemainingDots.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        _text.text = GameData.RemainingDots.ToString();
    }

}
