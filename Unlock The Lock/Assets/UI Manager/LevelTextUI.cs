﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelTextUI : MonoBehaviour
{

    public GameData GameData;
    private Text _text;

    // Use this for initialization
    void Start()
    {
        _text = GetComponent<Text>();
        updateText();
    }

    public void updateText()
    {
        _text.text = GameData.CurrentLevel.ToString();
    }
}
