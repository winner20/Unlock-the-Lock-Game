﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//#1

/**     Mark a ScriptableObject-derived type to be automatically listed in the Assets/Create
         submenu, so that instances of the type can be easily created and stored in the
         project as ".asset" files. 
	*/
[CreateAssetMenu]
    
/** game event handler
	summary : a list with type of eventlistener class + raise method to run the selected method "for all of listeners"
 */	
public class GameEvent : ScriptableObject
{
    List<EventListener> _eventListeners = new List<EventListener>();


/**
*   runs proper actions on events raise(will be called on paddle collide with dot)
 */
    public void Raise()
    {
        Debug.Log(this.name + " Raised");
        for (int i = 0; i < _eventListeners.Count; i++)
        {
            _eventListeners[i].OnEventRaised();
        }
    }

    public void Register(EventListener listener)
    {
        if (!_eventListeners.Contains(listener))
        {
            _eventListeners.Add(listener);
        }
    }

    public void DeRegister(EventListener listener)
    {
        if (_eventListeners.Contains(listener))
        {
            _eventListeners.Remove(listener);
        }
    }
}
