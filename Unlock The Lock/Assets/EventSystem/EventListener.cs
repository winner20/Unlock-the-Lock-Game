﻿using UnityEngine;
using UnityEngine.Events;

//#2

/**
* GUI Event listener handler to do something when a selected event happends
 */
public class EventListener : MonoBehaviour {
	public GameEvent[] EventsToListen     ; // game event = custom class with event schema methods

	public UnityEvent _responseToEvent ; // unity event instance_makes an event handler ui(to run a selected method)


	
	
	//Invoke all registered callbacks (runtime and persistent).
	public void OnEventRaised() {		_responseToEvent.Invoke (    );			} // this will run the selected function in ui(eventlistener ui)
	
	//This function is called when the object is loaded.(in gui)
	 void OnEnable()
    {
        foreach (GameEvent ev in EventsToListen) { ev.Register(this); }
    } // adds a listener to events manager
	//This function is called when the scriptable object goes out of scope.(in gui)
	void OnDisable()
    {
        foreach (GameEvent ev in EventsToListen) { ev.DeRegister(this); }
    } // removes a listener from events manager
	
}

