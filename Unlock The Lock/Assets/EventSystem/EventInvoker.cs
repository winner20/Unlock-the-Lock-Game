﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///Simple class to invoke an event manually(useful for animation events)
public class EventInvoker : MonoBehaviour
{

    public GameEvent _LoadNextLevelEvent;
    
    public GameEvent _SaveMeEvent;//only for hiding ad on savepage restart button click_didnt use new event for performance
    public void LoadNextLevelEvent()
    {
        _LoadNextLevelEvent.Raise();
    }
   public void SameMeEvent()
    {
        _SaveMeEvent.Raise();
    }
 
}
